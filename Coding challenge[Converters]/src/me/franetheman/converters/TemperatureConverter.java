package me.franetheman.converters;

public class TemperatureConverter {

    public TemperatureConverter() {

    }

    public double convertFromCelsius(TempType to, double amount) {
	switch (to) {
	case CELSIUS:
	    return amount;
	case FARENHEIT:
	    return amount*9/5 + 32;
	case KELVIN:
	    return amount*1+273.15;
	}
	return 0.0D;
    }

    public double convertFromFarenheit(TempType to, double amount) {
	switch (to) {
	case CELSIUS:
	    return Math.subtractExact((long)amount, (long)32) * 5/9;
	case FARENHEIT:
	    return amount;
	case KELVIN:
	    return Math.addExact((long)amount, (long)459.67) * 5/9;
	}
	return 0.0D;
    }

    public double convertFromKelvin(TempType to, double amount) {
	switch (to) {
	case CELSIUS:
	    return amount - 273.15;
	case FARENHEIT:
	    return amount * 9/5 - 459.67;
	case KELVIN:
	    return amount;
	}
	return 0.0D;
    }

    public enum TempType {
	CELSIUS, FARENHEIT, KELVIN;
    }
}
