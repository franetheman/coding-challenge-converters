package me.franetheman.converters;

public class DecimalToBinaryConverter {

    public DecimalToBinaryConverter() {
    }

    public String toBinary(int decimal) {
	if (decimal == 0) {
	    return "0";
	}
	String binary = "";
	while (decimal > 0) {
	    int rem = decimal % 2;
	    binary = rem + binary;
	    decimal = decimal / 2;
	}
	return binary;
    }
}
