package me.franetheman.converters;

public class CurrencyConverter {

    public CurrencyConverter() {

    }

    public double convert(Currency from, Currency to, double amount) {
	switch (from) {
	case EURO:
	    switch (to) {
	    case EURO:
		return amount* Currency.inEuro(from);
	    case US_DOLLAR:
		return amount* Currency.inUSDollar(from);
	    case BRITTISH_POUND:
		return amount* Currency.inBrittishPound(from);
	    case YEN:
		return amount* Currency.inYen(from);
	    }
	    break;
	case US_DOLLAR:
	    switch (to) {
	    case EURO:
		return amount* Currency.inEuro(from);
	    case US_DOLLAR:
		return amount* Currency.inUSDollar(from);
	    case BRITTISH_POUND:
		return amount* Currency.inBrittishPound(from);
	    case YEN:
		return amount* Currency.inYen(from);
	    }
	    break;
	case BRITTISH_POUND:
	    switch (to) {
	    case EURO:
		return amount* Currency.inEuro(from);
	    case US_DOLLAR:
		return amount* Currency.inUSDollar(from);
	    case BRITTISH_POUND:
		return amount* Currency.inBrittishPound(from);
	    case YEN:
		return amount* Currency.inYen(from);
	    }
	    break;
	case YEN:
	    switch (to) {
	    case EURO:
		return amount* Currency.inEuro(from);
	    case US_DOLLAR:
		return amount* Currency.inUSDollar(from);
	    case BRITTISH_POUND:
		return amount* Currency.inBrittishPound(from);
	    case YEN:
		return amount* Currency.inYen(from);
	    }
	    break;
	}
	return 0.0D;
    }

    public enum Currency {
	EURO(1, 1.08, 0.71, 132.04), US_DOLLAR(0.93, 1, 0.66,
		122.580), BRITTISH_POUND(1.41, 1.52, 1, 186.76), YEN(0.0076,
			0.0082, 0.0054, 1);

	private double euro;
	private double us_dollar;
	private double brittish_pound;
	private double yen;

	Currency(double euro, double us_dollar, double brittish_pound,
		double yen) {
	    this.euro = euro;
	    this.us_dollar = us_dollar;
	    this.brittish_pound = brittish_pound;
	    this.yen = yen;
	}

	public static double inEuro(Currency type) {
	    return type.euro;
	}

	public static double inUSDollar(Currency type) {
	    return type.us_dollar;
	}

	public static double inBrittishPound(Currency type) {
	    return type.brittish_pound;
	}

	public static double inYen(Currency type) {
	    return type.yen;
	}
    }
}
